﻿using L1_LINQ.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;


namespace L1_LINQ.BusinessLayer.Interfaces
{
    interface IUserService
    {
        IList<(User, IOrderedEnumerable<Task>)> OrderedUsersAndTasks();
        (User, Project, int?, int?, Task) InfoAboutTasksByUserId(int id);
    }
}
