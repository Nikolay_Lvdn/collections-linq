﻿using L1_LINQ.BusinessLayer.Interfaces;
using L1_LINQ.DataAccessLayer.Entities;
using L1_LINQ.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace L1_LINQ.BusinessLayer.Services
{
    class ProjectService : IProjectService
    {
        private readonly IData _data;

        public ProjectService(IData data)
        {
            _data = data;
        }

        //1
        public IDictionary<Project, int> CountOfTasksOfProjectsByUserId(int id)
        {
            var projects = _data.GetProjects();

            var result = projects.Where(project => project.authorId == id)
                .ToDictionary(project => project, project => project.Tasks.Count);

            if (result.Count == 0)
                throw new ArgumentException("User not author of any project");

            return result;
        }

        //7
        public IEnumerable<(Project, Task, Task, int)> InfoAboutProjects()
        {
            var tasks = _data.GetTasks();
            var projects = _data.GetProjects();
            var users = _data.GetUsers();

            /*var result = projects.GroupJoin(tasks,
                project => project.id,
                task => task.projectId,
                (project, tasks) =>
                (
                    Project: project,
                    LongestTask: tasks.OrderBy(t => t.description).LastOrDefault(),
                    ShortestTask: tasks.OrderBy(t => t.name).FirstOrDefault(),
                    CountOfMembers: users.Where(u => u.teamId == project.teamId).Count()
                ))
                .ToList();*/
            var result = projects.Where(project => project.description.Length>20 || project.Tasks.Count()<3)
                .GroupJoin(users,
                project => project.teamId,
                user => user.teamId,
                (project, users) => (Project: project, Users: users))
                .Select(data => (
                Project: data.Project,
                LongestTask: data.Project.Tasks.OrderBy(t => t.description).LastOrDefault(),
                ShortestTask: data.Project.Tasks.OrderBy(t => t.name).FirstOrDefault(),
                CountOfMembers: data.Users.Count()
                ));

            if (result.Count() == 0)
                throw new ArgumentException("There are no projects");

            return result;
        }
    }
}
