﻿using System;
using System.Collections.Generic;
using System.Linq;
using L1_LINQ.BusinessLayer.Interfaces;
using L1_LINQ.DataAccessLayer.Entities;
using L1_LINQ.DataAccessLayer.Interfaces;

namespace L1_LINQ.BusinessLayer.Services
{
    public class TaskService : ITaskService
    {
        private readonly IData _data;

        public TaskService(IData data)
        {
            _data = data;
        }

        //2
        public IList<Task> TasksByUserIdWithShortName(int id)
        {
            var tasks = _data.GetTasks();

            List<Task> result = tasks.Where(task => task.performerId == id && task.name.Length < 45).ToList();

            if (result.Count == 0)
                throw new ArgumentException("There are no such tasks");

            return result;
        }

        //3
        public IList<(int, string)> FinishedTaskByUserIdInThisYear(int id)
        {
            var tasks = _data.GetTasks();

            List<(int, string)> result = tasks.Where(task => task.performerId == id
                 && task.finishedAt != null
                 && task.finishedAt.Value.Year == DateTime.Now.Year)
                .Select(t => (Id: t.id, Name: t.name))
                .ToList();

            if (result.Count == 0)
                throw new ArgumentException("There are no such tasks");

            return result;
        }

    }
}
