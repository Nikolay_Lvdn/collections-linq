﻿using L1_LINQ.BusinessLayer.Interfaces;
using L1_LINQ.BusinessLayer.Services;
using L1_LINQ.DataAccessLayer;
using L1_LINQ.DataAccessLayer.Interfaces;
using L1_LINQ.DataAccessLayer.Repositories;
using L1_LINQ.PresentationLayer.Interfaces;
using Newtonsoft.Json;
using System;

namespace L1_LINQ.PresentationLayer.Views
{
    class ShowUI : IShowUI
    {
        private readonly Context _context = new Context();
        private readonly IData _data;
        private readonly IProjectService _projectService;
        private readonly ITaskService _taskService;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;
        public ShowUI()
        {
            _data = new Data(_context);
            _projectService = new ProjectService(_data);
            _taskService = new TaskService(_data);
            _teamService = new TeamService(_data);
            _userService = new UserService(_data);
        }

        private int GetUserId()
        {
            Console.WriteLine("Please enter user id:");
            var input =  Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            return input;
        }
        public void CountOfTasksOfProjectsByUserId()
        {
            var result = _projectService.CountOfTasksOfProjectsByUserId(GetUserId());

            foreach (var r in result)
            {
                Console.WriteLine("Project Id:" + r.Key.id + " Name: " + r.Key.name + " Count: " + r.Value);
            }
            Console.WriteLine();
        }

        public void TasksByUserIdWithShortName()
        {
            var result = _taskService.TasksByUserIdWithShortName(GetUserId());

            foreach (var r in result)
            {
                Console.WriteLine("User Id: " + r.performerId + " Task Id: " + r.id + " Name: " + r.name);
            }
            Console.WriteLine();
        }
        public void FinishedTaskByUserIdInThisYear()
        {
            var result = _taskService.FinishedTaskByUserIdInThisYear(GetUserId());

            foreach (var r in result)
            {
                Console.WriteLine("Task Id: " + r.Item1 + " Name: " + r.Item2);
            }
        }

        public void TeamsWithUsersOlderThan9()
        {
            var result = _teamService.TeamsWithUsersOlderThan9();

            foreach (var r in result)
            {
                Console.WriteLine("Team Id: " + r.Item1);
                Console.WriteLine("Team name: "+ r.Item2);
                Console.WriteLine(" Members:");
                foreach (var player in r.Item3)
                {
                    Console.WriteLine(" " + player.firstName+" "+player.lastName +" "+player.birthDay.Year+" "+ player.registeredAt);
                }
                Console.WriteLine();
            }
        }

        public void OrderedUsersAndTasks()
        {
            var result = _userService.OrderedUsersAndTasks();

            foreach (var r in result)
            {
                Console.WriteLine("User Id: " + r.Item1.id + " Name: " + r.Item1.firstName + r.Item1.lastName);
                Console.WriteLine(" Tasks:");
                foreach (var task in r.Item2)
                {
                    Console.WriteLine(" "+ task.id + " " + task.name);
                }
                Console.WriteLine();
            }
        }

        public void InfoAboutTasksByUserId()
        {
            var result = _userService.InfoAboutTasksByUserId(GetUserId());

            Console.WriteLine("User Id:" + result.Item1.id + " Name: " + result.Item1.firstName + result.Item1.lastName);
            Console.WriteLine("Last User Project Id: " + result.Item2?.id + " Name: " + result.Item2?.name);
            Console.WriteLine("Last Project Tasks Count: " + result.Item3);
            Console.WriteLine("Count Of Incomplete Or Canceled Tasks: " + result.Item4);
            Console.WriteLine("LongestTask Id: " + result.Item5?.id + " Name: " + result.Item5?.name);
        }

        public void InfoAboutProjects()
        {
            var result = _projectService.InfoAboutProjects();

            foreach (var r in result)
            {
                Console.WriteLine("Project Id: " + r.Item1.id + " Name: " + r.Item1.name);
                Console.WriteLine("Longest Task Id: " + r.Item2?.id + " Description: " + r.Item2?.description);
                Console.WriteLine("Shortest Task Id: " + r.Item3?.id + " Name: " + r.Item3?.name);
                Console.WriteLine("Count Of Members: " + r.Item4);
                Console.WriteLine();
            }
        }

        public void CommandMenu()
        {
            Console.WriteLine("1 - Count Of Tasks Of Projects By UserId");
            Console.WriteLine("2 - Tasks By User Id With Short Name");
            Console.WriteLine("3 - Finished Task By User Id In This Year");
            Console.WriteLine("4 - Teams With Users Older Than 9");
            Console.WriteLine("5 - Ordered Users And Tasks");
            Console.WriteLine("6 - Info About Tasks By User Id");
            Console.WriteLine("7 - Info About Projects");
            Console.WriteLine("exit - to exit the program");
        }
    }
}
