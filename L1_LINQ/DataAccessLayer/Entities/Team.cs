﻿using System;

namespace L1_LINQ.DataAccessLayer.Entities
{
    public class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }

    }
}
