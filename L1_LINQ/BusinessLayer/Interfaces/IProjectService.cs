﻿using L1_LINQ.DataAccessLayer.Entities;
using System.Collections.Generic;


namespace L1_LINQ.BusinessLayer.Interfaces
{
    interface IProjectService
    {
        IDictionary<Project, int> CountOfTasksOfProjectsByUserId(int id);
        IEnumerable<(Project, Task, Task, int)> InfoAboutProjects();
    }
}
