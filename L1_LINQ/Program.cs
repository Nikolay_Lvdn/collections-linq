﻿using L1_LINQ.PresentationLayer.Views;
using System;

namespace L1_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowUI showUI = new ShowUI();
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter command(? - to call /help):");
                Console.ForegroundColor = ConsoleColor.White;
                string input = Console.ReadLine();
                try
                {
                    Console.WriteLine();
                    switch (input)
                    {
                        case "1":
                            showUI.CountOfTasksOfProjectsByUserId();
                            break;
                        case "2":
                            showUI.TasksByUserIdWithShortName();
                            break;
                        case "3":
                            showUI.FinishedTaskByUserIdInThisYear();
                            break;
                        case "4":
                            showUI.TeamsWithUsersOlderThan9();
                            break;
                        case "5":
                            showUI.OrderedUsersAndTasks();
                            break;
                        case "6":
                            showUI.InfoAboutTasksByUserId();
                            break;
                        case "7":
                            showUI.InfoAboutProjects();
                            break;
                        case "exit":
                            return;
                        case "?":
                            showUI.CommandMenu();
                            break;
                    }
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n");
                }
            }
        }
    }
}
