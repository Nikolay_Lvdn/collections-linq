﻿using System;

namespace L1_LINQ.DataAccessLayer.Entities
{
    public class Task
    {
        public int id { get; set; }
        public int projectId { get; set; }
        public int performerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int state { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? finishedAt { get; set; }

        public User Performer { get; set; }
    }
}
