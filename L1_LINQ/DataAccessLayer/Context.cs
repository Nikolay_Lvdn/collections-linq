﻿using System.Collections.Generic;
using System.Linq;
using L1_LINQ.DataAccessLayer.Entities;

namespace L1_LINQ.DataAccessLayer
{
    class Context
    {

        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }

        public Context()
        {
            Projects = Endpoints.GetProjectsAsync();
            Tasks = Endpoints.GetTasksAsync();
            Teams = Endpoints.GetTeamsAsync();
            Users = Endpoints.GetUsersAsync();

            InitializeTasks();

            InitializeProjects();

        }

        private void InitializeTasks()
        {
            foreach (var t in Tasks)
            {
                t.Performer = Users.FirstOrDefault(u => u.id == t.performerId);
            }
        }

        private void InitializeProjects()
        {
            foreach (var p in Projects)
            {
                p.Tasks = Tasks.Where(t => t.projectId == p.id).ToList();
                p.Author = Users.FirstOrDefault(u => u.id == p.authorId);
                p.Team = Teams.FirstOrDefault(t => t.id == p.teamId);
            }
        }
    }
}
