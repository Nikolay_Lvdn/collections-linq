﻿using L1_LINQ.BusinessLayer.Interfaces;
using L1_LINQ.DataAccessLayer.Entities;
using L1_LINQ.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace L1_LINQ.BusinessLayer.Services
{
    class TeamService : ITeamService
    {
        private readonly IData _data;

        public TeamService(IData data)
        {
            _data = data;
        }

        //4
        public IList<(int, string, IOrderedEnumerable<User>)> TeamsWithUsersOlderThan9()
        {
            var teams = _data.GetTeams();
            var users = _data.GetUsers();

            var result = teams.GroupJoin(users,
                team => team.id,
                user => user.teamId,
                (team, users) =>
                (Id: team.id, Name: team.name, Members: users.OrderByDescending(user => user.registeredAt)))
                .Where(team => team.Members.All(member => DateTime.Now.Year - member.birthDay.Year > 10)).ToList();

            if (result.Count == 0)
                throw new ArgumentException("There are no such teams");

            return result;
        }
    }
}
