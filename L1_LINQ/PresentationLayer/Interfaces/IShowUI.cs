﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_LINQ.PresentationLayer.Interfaces
{
    interface IShowUI
    {
        void CountOfTasksOfProjectsByUserId();
        void TasksByUserIdWithShortName();
        void FinishedTaskByUserIdInThisYear();
        void TeamsWithUsersOlderThan9();
        void OrderedUsersAndTasks();
        void InfoAboutTasksByUserId();
        void InfoAboutProjects();
        void CommandMenu();
    }
}
