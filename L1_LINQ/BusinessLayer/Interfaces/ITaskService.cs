﻿using L1_LINQ.DataAccessLayer.Entities;
using System.Collections.Generic;

namespace L1_LINQ.BusinessLayer.Interfaces
{
    interface ITaskService
    {
        IList<Task> TasksByUserIdWithShortName(int id);
        IList<(int, string)> FinishedTaskByUserIdInThisYear(int id);
    }
}
