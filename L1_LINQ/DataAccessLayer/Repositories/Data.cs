﻿using System.Collections.Generic;
using L1_LINQ.DataAccessLayer.Entities;
using L1_LINQ.DataAccessLayer.Interfaces;

namespace L1_LINQ.DataAccessLayer.Repositories
{
    class Data : IData
    {
        private readonly Context _context;

        public Data(Context context)
        {
            _context = context;
        }

        public List<Project> GetProjects()
        {
            return _context.Projects;
        }

        public List<Entities.Task> GetTasks()
        {
            return _context.Tasks;
        }

        public List<Team> GetTeams()
        {
            return _context.Teams;
        }

        public List<User> GetUsers()
        {
            return _context.Users;
        }
    }
}
