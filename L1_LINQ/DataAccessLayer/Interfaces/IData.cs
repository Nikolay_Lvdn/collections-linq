﻿using System.Collections.Generic;
using L1_LINQ.DataAccessLayer.Entities;

namespace L1_LINQ.DataAccessLayer.Interfaces
{
    public interface IData
    {
        public List<Project> GetProjects();
        public List<Task> GetTasks();
        public List<Team> GetTeams();
        public List<User> GetUsers();
    }
}
