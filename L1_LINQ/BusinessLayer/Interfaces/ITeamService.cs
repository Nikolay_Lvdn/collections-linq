﻿using L1_LINQ.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace L1_LINQ.BusinessLayer.Interfaces
{
    interface ITeamService
    {
        IList<(int, string, IOrderedEnumerable<User>)> TeamsWithUsersOlderThan9();
    }
}
