﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using L1_LINQ.DataAccessLayer.Entities;
using Newtonsoft.Json;
using TaskEntitie = L1_LINQ.DataAccessLayer.Entities.Task;

namespace L1_LINQ.DataAccessLayer
{
    static class Endpoints
    {
        private static HttpClient client = new HttpClient();

        public static List<Project> GetProjectsAsync()
        {
            var url = "https://bsa21.azurewebsites.net/api/Projects";

            return JsonConvert.DeserializeObject<List<Project>>(GetAsyncByURL(url).Result);
        }

        public static List<TaskEntitie> GetTasksAsync()
        {
            var url = "https://bsa21.azurewebsites.net/api/Tasks";

            return JsonConvert.DeserializeObject<List<TaskEntitie>>(GetAsyncByURL(url).Result);
        }

        public static List<Team> GetTeamsAsync()
        {
            var url = "https://bsa21.azurewebsites.net/api/Teams";

            return JsonConvert.DeserializeObject<List<Team>>(GetAsyncByURL(url).Result);
        }

        public static List<User> GetUsersAsync()
        {
            var url = "https://bsa21.azurewebsites.net/api/Users";

            return JsonConvert.DeserializeObject<List<User>>(GetAsyncByURL(url).Result);
        }

        private static async Task<string> GetAsyncByURL(string url)
        {
            HttpResponseMessage response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }
    }
}
