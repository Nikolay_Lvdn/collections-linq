﻿using System;
using System.Collections.Generic;

namespace L1_LINQ.DataAccessLayer.Entities
{
    public class Project
    {
        public int id { get; set; }
        public int authorId { get; set; }
        public int teamId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime deadline { get; set; }
        public DateTime createdAt { get; set; }

        public ICollection<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
}
