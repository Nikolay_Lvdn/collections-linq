﻿using L1_LINQ.BusinessLayer.Interfaces;
using L1_LINQ.DataAccessLayer.Entities;
using L1_LINQ.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace L1_LINQ.BusinessLayer.Services
{
    class UserService : IUserService
    {
        private readonly IData _data;

        public UserService(IData data)
        {
            _data = data;
        }

        //5
        public IList<(User,IOrderedEnumerable<Task>)> OrderedUsersAndTasks()
        {
            var tasks = _data.GetTasks();
            var users = _data.GetUsers();

            var result = users.GroupJoin(tasks,
                user => user.id,
                task => task.performerId,
                (user, tasks) => (User: user,Tasks: tasks.OrderByDescending(t => t.name.Length)))
                .OrderBy(u => u.User.firstName)
                .ToList();

            if (result.Count == 0)
                throw new ArgumentException("There are no such teams");

            return result;
        }

        //6
        public (User, Project, int?, int?, Task) InfoAboutTasksByUserId(int id)
        {
            var tasks = _data.GetTasks();
            var projects = _data.GetProjects();
            var users = _data.GetUsers();

            var result = users.GroupJoin(projects,
                user => user.id,
                project => project.authorId,
                (user, projects) => (User: user, Projects: projects.OrderBy(project => project.createdAt)))
                .GroupJoin(tasks,
                data => data.User.id,
                task => task.performerId,
                (data, tasks) => (User: data.User, Projects: data.Projects,
                Tasks: tasks.OrderBy(task => task.finishedAt != null ? task.finishedAt - task.createdAt : DateTime.Now - task.createdAt)))
                .Select(data => (
                    User: data.User,
                    LastUserProject: data.Projects.LastOrDefault(),
                    LastProjectTasksCount: data.Projects.LastOrDefault()?.Tasks.Count,
                    CountOfIncompleteOrCanceledTasks: data.Tasks.Where(task => task.finishedAt == null || task.state == 3)?.Count(),
                    LongestTask: data.Tasks.LastOrDefault()
                ))
                .FirstOrDefault(data => data.User.id == id);

            if (result.User == null)
                throw new ArgumentException("There are no such user");

            return result;
        }
    }
}
